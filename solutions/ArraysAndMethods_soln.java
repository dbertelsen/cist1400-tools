// Create an example of an array that interacts with methods
// Print out statements to the screen that explain what the program is doing

import java.security.SecureRandom;

public class ArraysAndMethods_soln
{

    public final static int SIZE = 100;
    public final static int LUCKY_NUMBER = 5;

    public static void main(String[] args)
    {
    
    // create an array of SIZE ints
        int[] myArray = new int[SIZE];
    
    // print the array
    printArray(myArray);
    
    // fill the array with random ints using your method
    fillIntArrayWithRandom(myArray);
    
    // print the array
    printArray(myArray);
    
    // send the middle element of your array to the cubing function
    int cubedNumber = cubeFunction(myArray[myArray.length/2]);    
    
    // print out the value read directly from your array and compare it to the
    // value from the previous step
    System.out.printf("The middle number is %d and the cubed value is %d\n",
        myArray[myArray.length/2], cubedNumber);
    
    // print the array
    printArray(myArray);
    
    // find out how many times LUCKY_NUMBER shows up in the array using your
    // method and print the results

    System.out.printf("The LUCKY_NUMBER %d shows up %d times in the array\n",
        LUCKY_NUMBER, countOccurrencesInArray(myArray, LUCKY_NUMBER));

    
    } // end main
    
    
    
    
    
    // create a method that takes an int array and fills it with random values
    // from 0 through 9
    public static void fillIntArrayWithRandom(int[] theArray)
    {
        SecureRandom random = new SecureRandom();
        
        for (int i = 0; i < theArray.length; i++)
        {
            theArray[i] = random.nextInt(10);
        }
    }
    
    // create a method that takes an integer that prints and returns
    // the cube of it as an int
    public static int cubeFunction(int number)
    {
        return number * number * number;
    }
    
    
        
    // create a method that takes an int array and an integer; return how many
    // times the integers shows up in the array as an integer.
    public static int countOccurrencesInArray(int[] theArray, int target)
    {
        int count = 0;
        for (int val : theArray)
        {
            if (val == target)
            {
                count++;
            }
        }
        return count;
    }
    
    
    
    // create a method that prints out an array passed to it in rows of 10
    // elements; it should be able to handle any size array (blank line after print)
    // (Hint: use a for loop)
    public static void printArray(int[] theArray)
    {
        int pos = 0;
        for (int num : theArray)
        {
            if (++pos > 0 && pos % 10 == 1)
            {
                System.out.println();
            }
            System.out.print(num+" ");
        }
        
        // The following code prints out blank lines at the end
        if (pos % 10 != 1)
        {
            System.out.println();
        }
        System.out.println();
    }
    
} // end class
