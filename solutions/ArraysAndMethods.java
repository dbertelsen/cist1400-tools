// Lecture 18 - Arrays and Methods 10/31/2016

// Create an example of an array that interacts with methods
// Print out statements to the screen that explain what the program is doing

import java.security.SecureRandom;

public class ArraysAndMethods
{
    public static final int SIZE = 100;
    public static final int LUCKY_NUMBER = 5;

    public static void main(String[] args)
    {
        // create an array of SIZE ints
        int[] array = new int[SIZE];
        
        // print the array
        
        // fill the array with random ints using your method
        fillArrayRandom(array);
        
        // print the array
        printIntArray(array);
        
        // send the middle element of your array to the cubing function
        int cube = cubeInteger(array[array.length/2]);
        
        // print out the value read directly from your array and compare it to the
        // value from the previous step
        System.out.println("\nThe cube is: " + cube);
        
        // print the array
        printIntArray(array);
        
        // find out how many times LUCKY_NUMBER shows up in the array using your
        // method and print the results
        System.out.println("Lucky number " + LUCKY_NUMBER + " appears "
            + countAppearances(array, LUCKY_NUMBER) + " times.");
        
    }// end main
    
    // create a method that takes an int array and fills it with random values
    // from 0 through 9
    public static void fillArrayRandom(int[] theArray)
    {
       SecureRandom secrnd = new SecureRandom();
       
       for(int i = 0; i < theArray.length; i++)
       {
           theArray[i] = secrnd.nextInt(10);
       }
    }
    
    // create a method that takes an integer that prints and returns
    // the cube of it as an int
    public static int cubeInteger(int num)
    {
        return (int) Math.pow(num, 3);
    }
        
    // create a method that takes an int array and an integer; return how many
    // times the integers shows up in the array as an integer.
    public static int countAppearances(int[] theArray, int value)
    {
        int count = 0;
        for(int i : theArray)
        {
            if( i == value)
            {
                count++;
            }
        }
        return count;
    }
    
    // create a method that prints out an array passed to it in rows of 10
    // elements; it should be able to handle any size array (blank line after print)
    // (Hint: use a for loop)
        public static void printIntArray(int[] theArray)
    {
        for(int i=0; i < theArray.length; i++)
        {
            System.out.print(" " + theArray[i]);
            if(i % 10 == 9)
            {
                System.out.println();
            }
        }
    }
    
}// end class