// TicTacToe board

public class TicTacToeBoard
{
    private char[][] board = new char[3][3];
    
    public TicTacToeBoard()
    {
        for (int r = 0; r < board.length; r++)
        {
            for (int c = 0; c < board[r].length; c++)
            {
                board[r][c] = ' ';
            }
        }
    } // end constructor
    
    public boolean placeMove(int row, int col, char player)
    {
        if (board[row][col] != ' ')
        {
            return false; // spot has been taken
        }
        
        board[row][col] = player;
        
        return true;
        
    } // end placeMove
    
    public char gameOver(int turnCount)
    {
        for (int x = 0; x < 3; x++)
        {
            // check the rows
            if (board[x][0] == board[x][1] && board[x][0] == board[x][2])
            {
                return board[x][0];
            }
            // check the cols
            if (board[0][x] == board[1][x] && board[0][x] == board[2][x])
            {
                return board[0][x];
            }
        }
        
        // check the diags
        if ((board[0][0] == board[1][1] && board[0][0] == board[2][2])
                || (board[0][2] == board[1][1] && board[2][0] == board[2][0]) )
            {
                return board[1][1];
            }
        
        
        if (turnCount == 9)
        {
            return 'T';
        }
        
        return ' ';
    } // end gameOver method
    
    public void printBoard()
    {
        for (int r = 0; r < board.length; r++)
        {
            for (int c = 0; c < board[r].length; c++)
            {
                System.out.print(board[r][c]);
                if (c < 2)
                {
                    System.out.print('|');
                }
            }
            
            System.out.println();
            if (r < 2)
            {
                System.out.println("-----");
            }
            
        }
        System.out.println();
    }
}