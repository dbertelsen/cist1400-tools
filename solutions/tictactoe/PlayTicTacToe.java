import java.util.Scanner;
import java.security.SecureRandom;

public class PlayTicTacToe
{
    private static TicTacToeBoard tttboard;
    private static Scanner input;
    private static int turn = 1;
    private static char[] players = { 'O', 'X'};
    private static SecureRandom rand;
    
    public static void main(String[] args)
    {
        tttboard = new TicTacToeBoard();
        Scanner input = new Scanner(System.in);
        rand = new SecureRandom();
        
        do
        {
            tttboard.printBoard();
            
            System.out.print("Player " + players[turn % 2] + ": ");
            
            if (turn % 2 == 1)
            {
                int r;
                int c;
                
                do 
                {
                    System.out.print("Choose your move (row and col separated by a space): ");
                    r = input.nextInt();
                    c = input.nextInt();
                } while ((r > 2 || r < 0 || c > 2 || c < 0) || !tttboard.placeMove(r, c, players[1]));
            }
                
            else
            {
                System.out.println();
                int compRow;
                int compCol;
                do {
                    compRow = rand.nextInt(3);
                    compCol = rand.nextInt(3);
                    
                } while (!tttboard.placeMove(compRow, compCol, players[0]));
            }
            
            turn++;
            
            
        } while (tttboard.gameOver(turn) == ' ');

    }
}