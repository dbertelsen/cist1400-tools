// TicTacToe Test class

import java.util.Scanner;

public class TTTTest
{
    private static TicTacToeBoard tttboard;
    private static Scanner input;
    private static char player = 'X';
    
    public static void main(String[] args)
    {
        tttboard = new TicTacToeBoard();
        Scanner input = new Scanner(System.in);
        
        do
        {
            tttboard.printBoard();
            
            System.out.print("Player " + player
                + ": Choose your move (row and col separated by a space):");
            int r = input.nextInt();
            int c = input.nextInt();
            
            tttboard.placeMove(r, c, player);
            
            if (player == 'X')
            {
                player = 'O';
            }
            else
            {
                player = 'X';
            }
            
            
        } while (true);

    }
}