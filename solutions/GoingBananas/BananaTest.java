public class BananaTest
{
    public static void main(String[] args)
    {
        Banana b1 = new Banana();
        
        System.out.println("b1.length is: " + b1.getLength());
        System.out.println("b1.color is: " + b1.color);
        b1.color = "Lightly speckled golden yellow.";
        System.out.println("b1.color is: " + b1.color);
        b1.setLength(-100);
        System.out.println("b1.length is: " + b1.getLength());
        b1.setLength(18);
        System.out.println("b1 is " + b1.getLength() + " cm long and "
            + "looks " + b1.color);
        b1.ripen(11);
            
        System.out.println("b1 is " + b1.getLength() + " cm long and "
            + "looks " + b1.color);
        
        Banana b2 = new Banana(99);
        
        System.out.println("b2 is " + b2.getLength() + " cm long and "
            + "looks " + b2.color);

        BananaBunch bb1 = new BananaBunch(10, 0, "Dole");
        bb1.printBunch();
        
        bb1.pickBanana(5);
        
        bb1.printBunch();
        
        bb1.pickBanana(5);
        
    }
}
