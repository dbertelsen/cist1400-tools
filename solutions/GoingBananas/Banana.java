public class Banana
{
    private float length;
    String color;
    private int age;
    
    public Banana()
    {
        length = 10;
        color = "green";
        age = 0;
    }
    
    public Banana(float len)
    {
        length = lengthCheck(len);
        color = "green";
        age = 0;
    }
    
    public Banana(float len, int age)
    {
        length = lengthCheck(len);
        color = "green";
        age = Math.max(0, age);
    }
    
    public void setLength(int len)
    {
        if(len > 10)
        {
            length = len;
        }
    }
    
    public float getLength()
    {
        return length;
    }
    
    public void ripen(int days)
    {
        if(days > 0)
        {
            age += days;
        }
        
        if(age > 10)
        {
            color = "ROTTEN!";
        }
        
    }
    
    private float lengthCheck(float len)
    {
        if(len < 10 || len > 100)
        {
            length = 10;
        }
        else
        {
            length = len;
        }
        return length;
    }
    
    public int getAge()
    {
        return age;
    }
    
}