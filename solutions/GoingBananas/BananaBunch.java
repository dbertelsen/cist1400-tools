import java.security.SecureRandom;

public class BananaBunch
{
    private int age;
    private String brand;
    private boolean isInfested;
    private int count;
    Banana[] bananas;
    
    public BananaBunch(int num, int howOld, String brnd)
    {
        SecureRandom rnd = new SecureRandom();
        
        count = Math.max(0, num);
        brand = brnd;
        isInfested = false;
        age = Math.max(0, howOld);
        bananas = new Banana[count];
        
        for (int i = 0; i < bananas.length; i++)
        {
            bananas[i] = new Banana(10 + rnd.nextInt(20), age);
        }
    }
    
    public void printBunch()
    {
        for (int i = 0; i < bananas.length; i++)
        {
            if (bananas[i] == null)
            {
                System.out.println("Banana " + i +" missing");
            }
            else
            {
                System.out.printf("Banana %d is %d days old and %.2fcm long\n",
                    i, bananas[i].getAge(), bananas[i].getLength());
            }
        }
    }
    
    public Banana pickBanana(int position)
    {
        if (bananas[position] == null)
        {
            System.out.println("No banana for you!");
            return null;
        }
        
        Banana temp = bananas[position];
        bananas[position] = null;
        return temp;
    }
    
}