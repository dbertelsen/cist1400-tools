// Object to manage the dice

public class Hand
{
    private Die[] dice = new Die[5];
    
    public Hand()
    {
        for (int i = 0; i < dice.length; i++)
        {
            dice[i] = new Die(6);
        }
    } // end constructor

    public void rollAllDice()
    {
        for( Die die : dice)
        {
            die.roll();
        }
    } // end rollAllDice
    
    public void printHand()
    {
        System.out.println("Your hand is:");
        for (Die die : dice)
        {
            System.out.print(die.getValue() + " ");
        }
        System.out.println();
    } // end printHand()
    
    public int getDieValue(int position)
    {
        if (position >= 0 && position < dice.length)
        {
            return dice[position].getValue();
        }
        
        return -1;
        
    } // end getDieValue

} // end class