// A single die

import java.security.SecureRandom;

public class Die
{
    private int sides;
    private int value;
    private SecureRandom rand = new SecureRandom();
    
    public Die(int numSides)
    {
        sides = numSides;
        roll();
        
    } // end constructor
    
    // selects a random side, stores it in value and returns value
    public int roll()
    {
        value = rand.nextInt(sides) + 1;
        return value;
    } // end roll
    
    public int getValue()
    {
        return value;
    } // end getValue
    
    public int getSides()
    {
        return sides;
    } // end getSides
    
} // end class