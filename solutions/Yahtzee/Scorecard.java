// Class for a Yahtzee-style scorecard

public class Scorecard
{
    private int[] singleScores = {0, -1, -1, -1, -1, -1, -1};
    private String name;
    
    public Scorecard(String playerName)
    {
        name = playerName;
        //singleScores = {0, -1, -1, -1, -1, -1, -1};
    } // end constructor
    
    public int setSingleScores(Hand hand, int value)
    {
        // user has already used this slot on the scorecard
        if (singleScores[value] != -1)
        {
            return -1;
        }
        
        int score = 0;
        
        for (int pos = 0; pos < 6; pos++)
        {
            if (hand.getDieValue(pos) == value)
            {
                score += value;
            }
        }
        
        singleScores[value] = score;
        return score;
        
    } // end setSingleScore
    
    public int getSingleScores(int value)
    {
        return singleScores[value];
    } // end getSingleScore
    
    public void showCard()
    {
        System.out.println(" " + name + "'s Scorecard");
        System.out.println("==================================");
        System.out.println("   Roll             Score");
        System.out.println("   ACE              " + singleScores[1]);
        System.out.println("   TWO              " + singleScores[2]);
        System.out.println("   THREE            " + singleScores[3]);
        System.out.println("   FOUR             " + singleScores[4]);
        System.out.println("   FIVE             " + singleScores[5]);
        System.out.println("   SIX              " + singleScores[6]);
        System.out.println("==================================");
        System.out.println(" TOTAL:             " + getSingleScoreTotal());
    } // end showCard
    
    public int getSingleScoreTotal()
    {
        int total = 0;
        for (int score : singleScores)
        {
            if (score > 0)
            {
                total += score;
            }
        }
        if (total >= 63)
        {
            total += 35;
        }

        return total;
    }
    
} // end class