// Class to test the functionality of the Die class

public class DieTest
{
    private static Die die6;
    private static Die die20;
    private static Hand hand;
    private static Scorecard scorecard;
    
    public static void main(String[] args)
    {
    
        die6 = new Die(6);
        die20 = new Die(20);
        
        System.out.println("die6 has " + die6.getSides()
            + " sides and its value is " + die6.getValue());
            
        System.out.println("die20 has " + die20.getSides()
            + " sides and its value is " + die20.getValue());    
        
        
        int[] counts = new int[21];
        
        for (int i = 0; i < 1000; i++)
        {
            counts[die6.roll()]++;
        }
        
        for (int count : counts)
        {
            if (count != 0)
            {
                System.out.println(count);
            }
        }
        
        hand = new Hand();
        
        System.out.println("Create a new hand");
        hand.printHand();
        System.out.println("Roll all the dice");
        hand.rollAllDice();
        System.out.println("Print the hand twice");
        hand.printHand();
        hand.printHand();
        
        scorecard = new Scorecard("Billy");
        
        scorecard.showCard();
        
        scorecard.setSingleScores(hand, 3);
        
        hand.rollAllDice();
        hand.printHand();
        
        scorecard.showCard();
        
        scorecard.setSingleScores(hand, 3);
        
        scorecard.showCard();
        
        scorecard.setSingleScores(hand, 2);
        
        scorecard.showCard();
        
    } // end main
    
} // end class