// A demonstration of the .charAt() function in Java

import java.util.Scanner;

public class Charat
{
    private static Scanner input = new Scanner(System.in);
    
    // This variable chooses which letter to print
    private static final int POSITION = 2;
    
    public static void main(String[] args)
    {
        System.out.print("Enter some text and I'll"
            + " print out the third letter: ");
        char c = input.next().charAt(POSITION);
        
        System.out.printf("Your letter is %c.\n", c);
        
        // Print Winner! if the letter is a w
        
        if (c == 'w')
        {
            System.out.println("Winner!");
        }
        
    } // end main
} // end class