// Connect Four (TM) clone using 2D arrays


/* 
    Status: I have the game working except for checking wins.
    Moves have been changed to lower case; switch to upper case to mark a win.
    
    Hint for checking wins: you can do math on chars!
    x + x + x + x == 4x is true
    x + o + x + x != 4x is false
    
    Be careful with array ranges when checking!
    
*/
  

/* Basic board layout

| | | | | | | |
| | | | | | | |
| | | | | | | |
| | | | | | | |
| | |o| | | | |
| |x|x|o| | | |
===============
 0 1 2 3 4 5 6

 */
 
import java.util.Scanner;
 
public class FourInARow
{
    static Scanner input = new Scanner(System.in);
    static char[][] board = new char[6][7];
    
    public static void main(String[] args)
    {


        boolean gameWon = false; // remove this after we can check for a winner
        char player = 'x'; // the losing player always starts the next game
        
        // loop to repeat gameplay
        do
        {
            resetBoard(board);

            // loop to repeat moves
            do
            {
            
                printBoard(board);
    
                int nextMove = getMove(board);
                
                placeMove(board, nextMove, player);
                
                player = switchPlayer(player);
                
                // check if win - eventually move this to the while condition
                
            } while (!gameWon);
            
        } while (keepPlaying());
    } // end main
    
    public static char switchPlayer(char current)
    {
        if (current == 'x')
            {
                return 'o';
            }
            else
            {
                return 'x';
            }
    } // end switchPlayer
    
    public static void placeMove(char[][] array, int col, char symbol)
    {
        int row = array.length - 1; // start at the bottom of the board
        
        while (array[row][col] != ' ' && row >= 0)
        {
            row--; // move up the column until an empty space is found (guaranteed from getMove)
        }
        
        array[row][col] = symbol; // set the move on the board
    }
    
    
    public static int getMove(char[][] array)
    {
        do
        {
            System.out.print("\nPlease enter your play (0-6): ");
            int play = input.nextInt();
            if (play >= 0 && play < array[0].length && array[0][play] == ' ')
            {
                return play;
            }
            System.out.print("INVALID MOVE! ");
            
        } while(true);
    }
    
    public static void printBoard(char[][] array)
    {
        for (int row = 0; row < array.length; row++)
        {
            System.out.print("|");
            for (int col = 0; col < array[row].length; col++)
            {
                System.out.print(array[row][col] + "|");
            }
            System.out.println();
        }
        System.out.println("===============");
        System.out.println(" 0 1 2 3 4 5 6 ");
                
    } // end print board method
    
    public static void resetBoard(char[][] array)
    {
        for (int row = 0; row < array.length; row++)
        {
            for (int col = 0; col < array[row].length; col++)
            {
                array[row][col] = ' ';
            }
        }
    }
    
    
    public static boolean keepPlaying()
    {
        do
            {
            System.out.print("\nDo you want to play again? ");
                    char answer = input.next().charAt(0);
                    if (answer == 'y' || answer == 'Y') // might do toLower() later
                    {
                        return true;
                    }
                    else if (answer == 'n' || answer == 'N') // might do toLower() later
                    {
                        return false;
                    }
            } while (true);
    }
    
} // end class