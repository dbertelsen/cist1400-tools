public class TicTacToeBoard
{
    private char[][] board;
    private char winner;
    
    public TicTacToeBoard(char[][] brd)
    {
        board = new char[3][3];
        
        for (int r = 0; r < brd.length; r++)
        {
            for (int c = 0; c < brd[r].length; c++)
            {
                board[r][c] = brd[r][c];
            }
        }
    }
    
    public boolean checkBoard()
    {
        int r = -1, c = -1, d = -1;
        r = checkRows();
        c = checkCols();
        d = checkDiags();
        
        if (r > -1)
        {
            winner = board[r][0];
            for(int col = 0; col < board[r].length; col++)
            {
                board[r][col] = Character.toUpperCase(board[r][col]); // we haven't really talked about this
            }
        }
        else if (c > -1)
        {
            winner = board[0][c];
            for(int row = 0; row < board.length; row++)
            {
                board[row][c] = ("" + board[row][c]).toUpperCase().charAt(0); // this is based on class material
            }
        }
        else if (d > -1)
        {
            winner = board[1][1];
            if (d == 0)
            {
                for (int i=0; i < board.length; i++)
                {
                    String str = ("" + board[i][i]).toUpperCase();
                    board[i][i] = str.charAt(0);
                }
            }
            else if (d == 1)
            {
                for (int i=0; i < board.length; i++)
                {
                    String str = ("" + board[i][board.length - 1 - i]).toUpperCase();
                    board[i][board.length - 1 - i] = str.charAt(0);
                }
            }
        }
        
        
        System.out.printf("r = %d c = %d d = %d\n", r, c, d);
        
        return Math.max(r, Math.max(c, d)) > -1;
    }
    
    private int checkRows()
    {
        for (int r = 0; r < board.length; r++)
        {
            if (board[r][0] == board[r][1] && board[r][1] == board[r][2] && board[r][0] != ' ')
            {
                return r;
            }
        }
        return -1;
    }
    
    private int checkCols()
    {
        for (int c = 0; c < board[0].length; c++)
        {
            if (board[0][c] == board[1][c] && board[1][c] == board[2][c] && board[0][c] != ' ')
            {
                return c;
            }
        }
        return -1;
    }
    
    private int checkDiags()
    {
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ')
        {
            return 0;
        }
        else if (board[0][2] == board[1][1] && board[1][1] == board[0][2]  && board[1][1] != ' ')
        {
            return 1;
        }
        return -1;
    }
    
    public char getWinner()
    {
        return winner;
    }
    
    public void printBoard()
    {
        for (char[] row : board)
        {
            for(char c : row)
            {
                System.out.print(c);
            }
            System.out.println();
        }
    }
    
}