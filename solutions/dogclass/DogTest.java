public class DogTest
{
    public static void main(String[] args)
    {
        Dog myDog = new Dog("Sparky", "beagle");
        
        myDog.celebrateBirthday();
        
        System.out.println("myDog's name is " + myDog.getName()
            + " it is a " + myDog.getBreed());
        
        myDog.celebrateBirthday();
        
        System.out.println(myDog.bark());
    } // end main
        
} // end class
