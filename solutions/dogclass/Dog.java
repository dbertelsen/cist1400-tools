public class Dog
{
    private String name;
    private String breed;
    private int age;
    
    public Dog(String newName)
    {
        name = newName;
        age = 0;
    }
    
    public Dog(String newName, String newBreed)
    {
        name = newName;
        breed = newBreed;
        age = 0;
    }
    
    public String getName()
    {
        return name;
    }
    
    public String getBreed()
    {
        return breed;
    }
    
    public String bark()
    {
        return "Woof";
    }
    
    public void celebrateBirthday()
    {
        age++;
        System.out.println(name + " is now "
            + age + " years old!");
    }
} // end class