// L18 - Arrays and Methods - 10/31/2016

// Show how arrays are passed by reference to methods and cannot be
// replaced, only individual elements changed.

public class ArrayReference
{
    public static void main(String[] args)
    {
        // manually create an array holding 0-5
        int[] myArray = {0, 1, 2, 3, 4};
        
        // print the array out
        printIntArray(myArray);
        
        // call a method replaceArray on the array
        replaceArray(myArray);
        
        // print the array out
        printIntArray(myArray);
        
        // call a method replaceArrayElements on the array
        replaceArrayElements(myArray);
        
        // print the array out
        printIntArray(myArray);
        
        // call a method that returns a new array and set myArray to it
        myArray = getThreesArray(myArray);
        
        // print the array out
        printIntArray(myArray);

    }
    
    public static void replaceArray(int[] array)
    {
        // manually create an array2 containing 0,2,4,6,8 and
        // replace the entire array with array2
        int[] array2 = {0, 2, 4, 6, 8};
        array = array2; // this only works within a given method (scope)
        System.out.print("From replaceArray method: ");
        printIntArray(array);
    }
    
    public static void replaceArrayElements(int[] array)
    {
        // manually create an array2 containing 0,2,4,6,8 and
        // replace the individual elements with those from array2
        int[] array2 = {0, 2, 4, 6, 8};
        for (int i=0; i < array.length; i++)
        {
            array[i] = array2[i];
        }
    }
    
    public static int[] getThreesArray(int[] array)
    {
        int[] array2 = {0, 3, 6, 9, 12};
        return array2;
    }
    
    public static void printIntArray(int[] array)
    {
        System.out.print("[");
        for(int i : array)
        {
            System.out.print(" " + i);
        }
        System.out.println(" ]");
    }
}