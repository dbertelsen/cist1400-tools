public class TicTacToeTest
{
    private static TicTacToeBoard tttboard;
    
    public static void main(String[] args)
    {
        char[][] board = {{'x', 'x', 'x'}, {'o', ' ', 'o'}, {' ', 'o', ' '}};
        
        tttboard = new TicTacToeBoard(board);
        
        System.out.println("\nThe board before any changes:\n");
        
        tttboard.printBoard();
        
        System.out.println("\nChecking the board...\n");
        
        tttboard.checkBoard();
        
        System.out.println("\nThe board after checking for winners: (Winning moves are capitalized)\n");
        
        tttboard.printBoard();

        System.out.println("The winner was: " + tttboard.getWinner());
        
        
        
        char[][] board2 = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
        
        tttboard = new TicTacToeBoard(board2);
        
        System.out.println("\nThe board before any changes:\n");
        
        tttboard.printBoard();
        
        System.out.println("\nChecking the board...(should be no winners)\n");
        
        tttboard.checkBoard();
        
        System.out.println("\nThe board after checking for winners:\n");
        
        tttboard.printBoard();

        System.out.println("The winner was: " + tttboard.getWinner());
        
        
        
        char[][] board3 = {{'q', 'p', ' '}, {'q', ' ', ' '}, {'q', 'p', ' '}};
        
        tttboard = new TicTacToeBoard(board3);
        
        System.out.println("\nThe board before any changes:\n");
        
        tttboard.printBoard();
        
        System.out.println("\nChecking the board...(should be no winners)\n");
        
        tttboard.checkBoard();
        
        System.out.println("\nThe board after checking for winners:\n");
        
        tttboard.printBoard();

        System.out.println("The winner was: " + tttboard.getWinner());
        
        
        
        char[][] board4 = {{'x', ' ', 'o'}, {' ', 'x', ' '}, {'x', 'o', 'x'}};
        
        tttboard = new TicTacToeBoard(board4);
        
        System.out.println("\nThe board before any changes:\n");
        
        tttboard.printBoard();
        
        System.out.println("\nChecking the board...(should be no winners)\n");
        
        tttboard.checkBoard();
        
        System.out.println("\nThe board after checking for winners:\n");
        
        tttboard.printBoard();

        System.out.println("The winner was: " + tttboard.getWinner());
        
        
        
        char[][] board5 = {{'o', ' ', 'x'}, {' ', 'x', ' '}, {'x', 'o', 'x'}};
        
        tttboard = new TicTacToeBoard(board5);
        
        System.out.println("\nThe board before any changes:\n");
        
        tttboard.printBoard();
        
        System.out.println("\nChecking the board...(should be no winners)\n");
        
        tttboard.checkBoard();
        
        System.out.println("\nThe board after checking for winners:\n");
        
        tttboard.printBoard();

        System.out.println("The winner was: " + tttboard.getWinner());
        
    }
}