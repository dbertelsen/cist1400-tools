// L17 - Random numbers (and arrays) - 10 / 26 / 2016

// Create a simulation that will roll a die with a user-specified number of
// sides (make sure it is at least 2) 1000 times and return the results for each
// side as a count and percentage of the total. Output should be formatted as
// below; you may see some offsets in your text alignment as number of sides
// increases. (You could fix this with printf...)

/* Example:

   How many sides are on your die? _6_

   Results for 1000 rolls of a 6-sided die:
   Side   Rolls   Percent
   1      152     15.20 %
   2      165     16.50 %
   3      170     17.00 %
   4      163     16.30 %
   5      172     17.20 %
   6      178     17.80 %
*/

import java.security.SecureRandom;
import java.util.Scanner;

public class Dice
{
   private static SecureRandom rnd = new SecureRandom();
   private static Scanner input = new Scanner(System.in);
   private static final int ROLLS = 1000;
   
   public static void main(String[] args)
   {
      // get valid user input
      int numSides;
      
      do
      {
         System.out.print("How many sides are on your die? ");
         numSides = input.nextInt();
      } while (numSides < 1);
      
      
      // create correctly-sized array
      // TODO: make different sized later
      int[] results = new int[numSides + 1];
      
      // roll the dice 1000 times
      for(int roll = 1; roll <= 1000; roll++)
      {
         results[rnd.nextInt(numSides) + 1]++;
      }
      
      // print out the results
      printResults(results);
   }
   
   private static void printResults(int[] array)
   {
      System.out.println("Results for " + ROLLS + " rolls "
         + "of a "+ (array.length - 1) + "-sided die.");
      System.out.println("Side   Rolls   Percent");
         
      for(int i = 1; i < array.length; i++)
         {
            System.out.printf("%-6d %-6d %5.2f %%\n",
               i, array[i], 100.0*array[i]/ROLLS);
         }
   }
}
