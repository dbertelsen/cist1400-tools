#!/bin/bash

# Static file checker for UNO CIST1400 problems

HOME=/home/ubuntu/workspace/cist1400-tools

PROG_DIR=$HOME/../repository/homework
STYLE_DIR=$HOME/styles
CS_BIN=$HOME/"checkstyle-6.14.1-all.jar"
filename="default"

# Text formatting
RED='\033[01;31m' # Red
NC='\033[0m' # No Color

while [ true ]

do
    echo 
    echo "     **Welcome to the CIST1400 Code Style Checker**"
    echo "========================================================="
    echo "   1 - Punch and Run"         "         11 - Math (Geometry)"
    echo "   2 - Simple Output"         "         12 - Octal to Decimal"
    echo "   3 - Basic Math"            "            13 - Fibonacci"
    echo "   4 - Multiples"             "             14 - Perfect Numbers"
    echo "   5 - Smallest and Largest"  "  15 - Birthday Statistics"
    echo "   6 - Roman Numerals I"      "      16 - Triplet Sexy Primes"
    echo "   7 - Octal Conversion"      "      17 - Magic Squares"
    echo "   8 - Roman Numerals II"     "     18 - Conway's Life"
    echo "   9 - Shipping Calculator"   "   19 - Sudoku Checker"
    echo "  10 - Numerology"            "            20 - EC1 - ASCII Art"
    echo "  99 - QUIT" "21 - Magic2"
    echo
    
    printf "Program # to check: "
    read choice
    
    case $choice in
         1) filename="PunchRun";;
         2) filename="SimpleOutput";;
         3) filename="BasicMath";;
         4) filename="Multiple";;
         5) filename="SmallestLargest";;
         6) filename="Roman";;
         7) filename="OctalConversion";;
         8) filename="RomanLoop";;
         9) filename="Shipping";;
        10) filename="Numerology";;
        11) filename="Mathstuff";;
        12) filename="Decimal";;
        13) filename="Fibonacci";;
        14) filename="Perfect";;
        15) filename="Birthdays";;
        16) filename="Sieve";;
        17) filename="Magic";;
        18) filename="Life";;
        19) filename="SudokuChecker";;
        20) filename="ASCIIart";;
        21) filename="Magic2";;
        99 | "q" | "Q"  | "x" | "X" | "exit" | "EXIT" | "quit" | "QUIT")
            echo "Goodbye!";
            exit;;
        *) echo -e ${RED}"ERROR:${NC} Invalid choice, select again"; continue;;
    esac
        
    if [ -f $PROG_DIR/*_$filename.java ]; then
        echo "Checking "$filename"..."
        java -jar $CS_BIN -c $STYLE_DIR/$filename.xml $PROG_DIR/*_$filename.java
        echo "Press a key to continue"
        read -s -n 1
        clear
    else
        echo -e ${RED}"ERROR:${NC} netID_"$filename".java file not found!"
    fi

done
