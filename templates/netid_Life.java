// PUT YOUR HEADER DOCUMENTATION HERE!




public class netid_Life 
{

    // the size of the grid(GRIDSIZE x GRIDSIZE)
    static final private int GRIDSIZE = 18;
    private static Scanner sc = new Scanner(System.in);

    // No code in the main method should be changed(style changes ok)
    // ********************************************************************** //
    public static void main(String args[])
    {
        boolean[][] board = new boolean[GRIDSIZE][GRIDSIZE];
        char choice;
        int x = 1;

        do
        {
            System.out.print("Start with a (r)andom board, the (q)ueen bee shuttle or the (g)lider pattern? ");
            choice = sc.next().charAt(0);
        } while(choice != 'r' && choice != 'q' && choice != 'g');

        clearGrid(board);
        setup(board,choice);

        do
        {
            System.out.printf("Viewing generation #%d:\n\n", x++);
            displayGrid(board);
            genNextGrid(board);
            System.out.print("\n(q)uit or any other key + ENTER to continue: ");
            choice = sc.next().charAt(0);
        } while(choice != 'q');
    }

    // No code in the setup method should be changed (style changes ok)
    // ********************************************************************** //
    public static void setup(boolean[][] board, char which)
    {
        SecureRandom randomNumbers = new SecureRandom();

        clearGrid(board);

        if(which == 'q')
        {
            // Set up the Queen Bee Shuttle pattern
            board[5][1] = true; board[5][2] = true; board[6][3] = true;
            board[7][4] = true; board[8][4] = true; board[9][4] = true;
            board[10][3] = true; board[11][2] = true; board[11][1] = true;        
        }
        else if(which == 'g')
        {
            // Set up a Glider
            board [17][0] = true; board[16][1] = true; board[15][1] = true;
            board[16][2] = true; board [17][2] = true;
        }
        else
        {
            // set up random
            for(int row = 0; row < board.length; row++)
            {
                for(int col = 0; col < board[row].length; col++)
                {
                    if(randomNumbers.nextInt() % 2 == 0)
                        board[row][col] = true;
                }
            }
        }
    }

    // No code in the displayGrid method should be changed (style changes ok)
    // ********************************************************************** //
    public static void displayGrid(boolean[][] grid)
    {
        // Start printing the top row of numbers
        System.out.print("   ");
        for(int x = 1; x <= grid.length; x++)
        {
            if((x / 10) != 0)
                System.out.printf("%d", x / 10);
            else
                System.out.print(" ");
        }

        System.out.println();
        System.out.print("   ");

        for(int x = 1; x <= grid.length; x++)
        {
            System.out.printf("%d", x % 10);
        }
        System.out.println();

        for(int r = 0; r < grid.length; r++)
        {
            System.out.printf("%d", r+1);
            if(r + 1 < 10)
                System.out.print("  ");
            else
                System.out.print(" ");
            for(int c = 0; c < grid.length; c++)
            {
                if(grid[r][c] == true)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }


    // ********************************************************************** //

    // put the three methods you must write here and make sure to document them!

    public static void clearGrid(boolean[][] grid)
    {
        
    }

    public static void genNextGrid(boolean[][] grid)
    {
        
    }

    public static int countNeighbors(
        final boolean[][] grid, final int row, final int col)
    {
        
    }
}
