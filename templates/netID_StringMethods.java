// L22 - 04/03/2017 - String objects and the String class methods.

/* Explore the String class methods. Javadoc will be helpful.

You will be looking at the output of many of the String class methods. MAKE SURE
YOUR OUTPUT DOCUMENTS what method call is performing the work. See the example!
Results of each should be printed to the screen. There should be a blank line
between each exercise in the output.

*/

public class netID_StringMethods
{
    public static void main(String[] args)
    {
        
        // Initialize a String from a char[] and print it out.
        char[] myArray = {'h', 'e', 'l', 'l','o', ' ', 'w', 'o', 'r', 'l', 'd'};
        String chararray = new String(myArray);
        System.out.println("A string created from a char array: " + chararray);
        System.out.println();
        
        // Initialize a new empty string.
        
        
        // Initialize a string from the above char[] myArray that only includes the
        // word 'world' (no other methods at this time).
        
        
        // Create a string and print out the length of the string.
        
        
        // Find the third letter in a string.
        
        
        // See if "apple" equals "orange" and if "apple" equals "apple".
        
        
        // See if "apple" equals "apple" and if "apple" equals "Apple" if you
        // ignoreCase?
        
        
        // What results if you run compareTo between "apple" and "ape",
        // "apple" and "apple", "apple" and "apples"?
        
        
        // What is the index of the first 'e' in "supercalifragilisticexpialidocious"? 
        
        
        // What is the index of the last 'e' in "supercalifragilisticexpialidocious"?
        
        
        // Print out the substring "docious" from the above string.
        
        
        // Print out the substring "fragilistic" from the above string.
        
        
        // Replace all of the 'i's in the above string with 'I's.
        
        
        // Convert "supercalifragilisticexpialidocious" to all upper case.
        
        
        // Convert "LASER" to all lower case.
        
        
        // Remove the extra whitespace in the string "     It's full of stars!   "
        
        
        
        // Do three other clever things with strings and document them.
        
        
        
        // TODO - Push your results to your repository.
        
    } // end main
} // end class