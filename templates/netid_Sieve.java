// TODO - insert your personalized honor pledge

import java.util.Scanner;

public class netid_Sieve
{
    private static final int HOWMANY = 50000; // The range of numbers
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args)
    {

        // NOTE: You are not allowed to change any code in the main method!

        boolean[] sieve = new boolean[HOWMANY + 1]; // Boolean array

        int lower = 1, upper = HOWMANY; // Setting initial boundaries

        // the following method call will implement the Sieve algorithm

        processSieve(sieve);

        // the following method call will print all 330 sexy triplets of primes

        showPrimes(sieve, lower, upper);

        // the following method call gets the lower boundary for printing

        lower = getLower(HOWMANY);

        // the following method call gets the upper boundary for printing

        upper = getUpper(lower, HOWMANY);

        // the following method call prints sexy triplets in the new range

        showPrimes(sieve, lower, upper);

    } // end main


    /**
     * Short description here
     * 
     * DESCRIBE, IN DETAIL, WHAT YOUR METHOD DOES HERE
     *
     * @param  theArray   the boolean array to process
     */

    // TODO - implement the processSieve method and remove this comment


    /**
     * Short description here
     *
     * DESCRIBE, IN DETAIL, WHAT YOUR METHOD DOES HERE
     *
     * @param  theArray    the boolean array to process
     * @param  bottom      the bottom position of the range to display
     * @param  top         the top position of the range to display
     */

    // TODO - implement the showPrimes method and remove this comment


    /**
     * Short description here
     *
     * DESCRIBE, IN DETAIL, WHAT YOUR METHOD DOES HERE
     *
     * @param  maximumValue   the largest value to allow
     * @return                an integer value, which is the lower boundary
     */

    // TODO - implement the getLower method and remove this comment


    /**
     * Short description here
     *
     * DESCRIBE, IN DETAIL WHAT YOUR METHOD DOES HERE
     *
     * @param  minValue    the smallest value to allow
     * @param  maxValue    the largest value to allow
     * @return             an integer value, which is the upper boundary
     */

    // TODO - implement the getUpper method and remove this comment


} // end class
