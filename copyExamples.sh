#!/bin/bash

# Lecture example copier for UNO CIST1400

if (cat ~/.bash_aliases | grep copyExamples); then
    echo
else
    echo "Adding shortcuts to .bash_aliases..."
    echo 'alias copyExamples="~/workspace/cist1400-tools/copyExamples.sh"' >> ~/.bash_aliases
    echo "Alias added. You may just type 'copyExamples' after you have restarted your terminal."
fi

# Text formatting
RED='\033[01;31m' # Red
NC='\033[0m' # No Color

PRAC_SRC=~/workspace/cist1400-tools/practice

PRAC_DIR=~/workspace/repository/practice

TEMP_SRC=~/workspace/cist1400-tools/templates

HMWK_DIR=~/workspace/repository/homework

# Create practice directory if needed
if [ ! -d $PRAC_DIR ]
then
    mkdir $PRAC_DIR
fi

while [ true ]
do
    echo
    echo "    **Welcome to the CIST1400 Lecture Example Copier**"
    echo "=========================================================="
    echo "   1 - Java Basics"           "           13 - Modules"
    echo "   2 - Memory and Arithmetic" " 14 - Methods and Documentation"
    echo "   3 - Selection"             "             15 - Arrays"
    echo "   4 - Conditional Operators" " 16 - More For Loops"
    echo "   5 - Logical Operators"     "     17 - Random Numbers"
    echo "   6 - Switch Statements"     "     18 - Passing Arrays"
    echo "   7 - While (Counter)"       "       19 - Search and Sort"
    echo "   8 - While (Sentinel)"      "      21 - 2D Arrays"
    echo "   9 - Incrementing"          "          22 - Strings"
    echo "  11 - For and do/while loops"      #"      20 - "
    echo "  116 - P16 - Sieve of Eratosthenes boilerplate code"
    echo "  118 - P18 - Conway's Life boilerplate code"
    echo "  99 - QUIT"
    echo

    printf "Lecture # to copy or 99 to quit: "
    read choice

    case $choice in
         1)
            cp -i $PRAC_SRC/broken01.java $PRAC_DIR
            cp -i $PRAC_SRC/broken02.java $PRAC_DIR
            cp -i $PRAC_SRC/broken03.java $PRAC_DIR
            cp -i $PRAC_SRC/helloworld.java $PRAC_DIR
            cp -i $PRAC_SRC/subtraction.java $PRAC_DIR
            ;;
         2)
            cp -i $PRAC_SRC/OrderOps.java $PRAC_DIR
            cp -i $PRAC_SRC/IntDivision.java $PRAC_DIR
            cp -i $PRAC_SRC/IntModulus.java $PRAC_DIR
            ;;
         3)
            cp -i $PRAC_SRC/IfAbsolute.java $PRAC_DIR
            cp -i $PRAC_SRC/IfElseTest.java $PRAC_DIR
            cp -i $PRAC_SRC/IfTest.java $PRAC_DIR
            ;;
         4)
            cp -i $PRAC_SRC/ConditionalOperators.java $PRAC_DIR
            ;;
         5)
            cp -i $PRAC_SRC/LogicalOperators.java $PRAC_DIR
            cp -i $PRAC_SRC/NetID_LogicTest.java $PRAC_DIR
            ;;
         6)
            cp -i $PRAC_SRC/Switch.java $PRAC_DIR
            ;;
         7)
            cp -i $PRAC_SRC/LoopIntegers.java $PRAC_DIR
            cp -i $PRAC_SRC/LoopMultiples.java $PRAC_DIR
            ;;
         8)
            cp -i $PRAC_SRC/PowersTwo.java $PRAC_DIR
            cp -i $PRAC_SRC/Sentinel.java $PRAC_DIR
            ;;
         9)
            cp -i $PRAC_SRC/Increment.java $PRAC_DIR
            ;;
        10)
            ;;
        11)
            ;;
        12)
            cp -i $PRAC_SRC/BreakContinue.java $PRAC_DIR
            ;;
        13)
            ;;
        14)
            cp -i $PRAC_SRC/Overload1.java $PRAC_DIR
            cp -i $PRAC_SRC/OverloadingAndDoc.java $PRAC_DIR
            ;;
        15)
            cp -i $PRAC_SRC/MethodsCube.java $PRAC_DIR
            cp -i $PRAC_SRC/TimeForArrays.java $PRAC_DIR
            ;;

        16)
            cp -i $PRAC_SRC/RightTri.java $PRAC_DIR    
            cp -i $PRAC_SRC/Isoceles.java $PRAC_DIR
            ;;
        17)
            cp -i $PRAC_SRC/Dice.java $PRAC_DIR
            ;;
        18)
            cp -i $PRAC_SRC/ArraysAndMethods.java $PRAC_DIR
            ;;
        19)
            cp -i $PRAC_SRC/Search_binary.java $PRAC_DIR
            cp -i $PRAC_SRC/Search_linear.java $PRAC_DIR
            cp -i $PRAC_SRC/Sort_bubble.java $PRAC_DIR
            cp -i $PRAC_SRC/Sort_selection.java $PRAC_DIR            
            ;;
        21)
            cp -i $PRAC_SRC/TwoDArrayPrint.java $PRAC_DIR
            ;;
        22)
            cp -i $TEMP_SRC/netID_StringMethods.java $PRAC_DIR
            ;;
        116)
            cp -i $TEMP_SRC/netid_Sieve.java $HMWK_DIR
            ;;
        118)
            cp -i $TEMP_SRC/netid_Life.java $HMWK_DIR
            ;;
            
        99 | "q" | "Q"  | "x" | "X" | "exit" | "EXIT" | "quit" | "QUIT")
            echo "Goodbye!";
            exit;;
        *) echo -e ${RED}"ERROR:${NC} Invalid choice, select again"; continue;;
    esac
    echo "Don't forget to add your files to the git repository!"

done