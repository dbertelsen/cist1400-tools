#!/bin/bash

toolsDir=$(pwd)

echo "*** This script will set up a workspace with several nice features; run it only once! ***"

echo "If you do not wish to continue, press <Ctrl>+c to exit"
read temp

echo "Checking for java compiler..."

if hash javac 2>/dev/null;then
    echo "Java compiler already installed"
else
    echo "Adding Java compiler..."
    sudo apt-get update
    sudo apt-get -y install openjdk-7-jdk
fi
echo

echo "Adding folders for for cist1400-practice..."
cd ..
ln -s $toolsDir/practice ..
cd $toolsDir

if (cat ~/.bash_aliases | grep checkstyle); then
    echo "Alias already exists in .bash_aliases."
else
    echo "Adding shortscuts to .bash_aliases..."
    echo 'alias checkstyle="~/workspace/cist1400-tools/checkstyle.sh"' >> ../../.bash_aliases
    # BROKEN # echo 'alias turnin="git push ~/workspace/homework"' >> ../../.bash_aliases
    echo "Aliases added. They will be active after you have restarted your terminal."
fi

echo
echo "Displaying public key so you can access your BitBucket repositories without a password..."
echo
echo "======================================================================================="
cat ~/.ssh/id_rsa.pub
echo "======================================================================================="
echo
echo "Copy the text between the lines and add it to your BitBucket profile under SSH Keys."
echo -n "Press enter when this is complete..."
read temp

echo
echo "Setting up your homework repository. Please select your repository on BitBucket,"
echo "click on 'Clone' and make sure the SSH option is selected instead of HTTPS. Copy the"
echo "command given there and paste it below and press enter."
echo
echo -n "BitBucket SSH clone command: "
read cloneCommand
echo

cd ..

$($cloneCommand repository)

cd repository

# Should add conditional to test if .gitignore exists
echo


echo "Making directories to help you organize things..."
mkdir homework
mkdir grading
mkdir practice

echo "Adding .gitignore to homework repository and running commit/push..."
cp $toolsDir/Java.gitignore .gitignore

git add .gitignore
git commit -a -m "Automatic addition of .gitignore"
git push

cd $toolsDir

echo
echo "If there are no warnings above, you are ready to begin!"
