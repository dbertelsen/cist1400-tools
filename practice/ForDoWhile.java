// Lecture 12 Review - 10/5/2016

// Show your programming skills with repetition

import java.util.Scanner;

public class ForDoWhile
{
    public static void main(String[] args)
    {
        System.out.println("\n*** Example of a do-while loop ***");
        System.out.println("**********************************\n");

        // write a do-while loop that prompts for an integer
        // between 1 and 100 (inclusive).
        
        
        System.out.println("\n*** Example of a for loop ***");
        System.out.println("*****************************\n");
        
        // write a for loop that prints the multiples of the number taken above
        // from 0 to 1,000 (inclusive).
        
        
    } // end main
} // end class