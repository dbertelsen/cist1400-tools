// L14 - Advanced Method Overloading - 2/27/2017

// Create two methods to calculate tax, one will accept an amount and one tax
// rate, the other will accept two tax rates

// Then, create Javadoc for your code!

import java.util.Scanner;

public class OverloadingAndDoc
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        double stateTaxRate;
        double localTaxRate;
        double itemValue;

        System.out.println("Welcome to the tax calculator\n"
            + "You may exit by entering zero for the item value.\n");

        do{
            itemValue = 0;
            stateTaxRate = 0;
            localTaxRate = 0;
            
            System.out.print("Enter the value of the item: ");
            itemValue = input.nextDouble();

            // Prompt user for state tax if the item has value
            if (itemValue > 0)
            {
                System.out.print("Enter the state tax rate (%): ");
                stateTaxRate = input.nextDouble();
            }
            // Only prompt for local tax if there the value > 0




            if (itemValue > 0)
            {
                if (localTaxRate == 0) // case where there is only state tax
                {
                    System.out.println("--Using the single tax rate method");
                    System.out.printf("Item value: $%f + $%f tax = $%f\n",
                        itemValue, calculateTax(itemValue, stateTaxRate),
                        itemValue + calculateTax(itemValue, stateTaxRate) );
                }
                else // we have both state and local taxes
                {
                    
                }
            }
        } while (itemValue > 0);
    } // end main

    // method for state tax only
    public static double calculateTax(double amount, double rate)
    {
        return rate/100 * amount;
    }

    // method for state and local tax
    public static double calculateTax(double amount, double rate1, double rate2)
    {
        return 0;
    }
    
} // end class
