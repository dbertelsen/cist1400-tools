// L21 - 3/29/2017 - Using 2-D arrays

// Write a method that will print out 2 dimensional arrays

public class TwoDArrayPrint
{
    private static final int[][] values = {{1,2,3}, {4,5,6}, {7, 8, 9}};
    
    private static final int[][] jagged = {{1,2,3}, {4,5}, {7, 8, 9, 0}};
    
    public static void main(String[] args)
    {
        print2DArray(values);
        
        System.out.println();
        
        // Uncomment the next line once you have the first array printing correctly
        // print2DArray(jagged);
        
    } // end main
    
    // TODO - write a method that prints a 2D array out

    
} // end class
    
/* Expected output
    Note: there is a leading space and no space at the end of each row.)
   
 1 2 3
 4 5 6
 7 8 9

 1 2 3
 4 5
 7 8 9 0
 
 */