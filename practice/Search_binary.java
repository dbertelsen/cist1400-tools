// L19 - Searching and Sorting - 3/27/2017

// Implement a binary search algorithm.

// Your search value of 23 was first found at index 6.

// The function will return -1 if the value was not found.

// BONUS: Add in functionality to allow use to enter a value to search for.

import java.security.SecureRandom;

public class Search_binary
{
    
    public static void main(String[] args)
    {
        int[] myArray = { 0, 2, 5, 7, 8, 14, 23, 31, 33, 36, 38, 42, 47, 50, 51,
            54, 59, 63, 68, 75, 77, 78, 79, 84, 87, 89, 90, 91, 93, 95, 97, 100}; 
        SecureRandom rand = new SecureRandom();

        printArray(myArray);
        System.out.println();
        
        int target = rand.nextInt(101);
        
        // TODO - Add code to print out search results as described above.
        

    } // end main
    
    public static int binarySearch(int[] values, int searchKey)
    {
        int comparisionCount = 0;
        
        
        
        System.out.printf("comparisonCount=%d\n", comparisionCount);
        return 0;
    } // end binarySearch method
    
    public static void printArray(int[] values)
    {
        System.out.print("Array Contents: [");
        for (int value : values)
        {
            System.out.printf(" %d,", value);
        }
        System.out.println(" ]");
        
    } // end printArray method
    
} // end class