// L13 - Intro to overloading - 2/27/2017
// Two methods for printing the max of integers

// Create a method to compare two integers and return the max.
// Then use that method in another method to find the max of more integers

public class Overload1
{
    public static void main(String[] args)
    {
        int num1 = 10;
        int num2 = 20;
        int num3 = 30;
        int num4 = 40;
        
        System.out.print("The max of %d and %d is %d",
            num1, num2, getMax(num1, num2) );
            
    } // end main
    
    
} // end class