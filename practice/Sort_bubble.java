// L19 - Searching and Sorting - 3/7/2017

// Implement a bubble sort algorithm.
// It should create an array of length SIZE and fill it with positive integer
// values up to SIZE squared. It should then sort using bubble sort and print
// the array after each sort iteration and the number of comparisons needed

import java.security.SecureRandom;

public class Sort_bubble
{
    private static final int SIZE = 16;
    
    
    public static void main(String[] args)
    {
        int[] myArray = new int[SIZE];
        SecureRandom rand = new SecureRandom();

        System.out.print("Before sorting: ");
        printArray(myArray);
        System.out.println();
        
        // call the bubbleSort method on your array
        
        
        System.out.print("After sorting: ");
        printArray(myArray);
        System.out.println();
        
        

    } // end main
    
    public static void bubbleSort(int[] values)
    {
        int comparisionCount = 0;
        
        // TODO - sort the array and print it after a value is moved to its
        // final position
                
        System.out.printf("comparisonCount=%d\n", comparisionCount);
    } // end bubbleSort method
    
    
    public static void printArray(int[] values)
    {
        System.out.print("Array Contents: [");
        for (int value : values)
        {
            System.out.printf(" %d,", value);
        }
        System.out.println(" ]");
        
    } // end printArray method
    
} // end class