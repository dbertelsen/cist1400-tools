// Lecture 12 - 02/20/2017

// Write a for loop that contains a conditional statement within the body
// The first example should break when that is true; the second should
// continue when the condition is true. Make sure you have some output to
// reflect the difference in behavior.

public class BreakContinue
{
    public static void main(String[] args)
    {
        // For with conditional break
        
        
        
        // Same for with conditional continue
        
        
        
    } // end main
} // end class
