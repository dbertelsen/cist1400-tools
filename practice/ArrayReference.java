// L18 - Arrays and Methods - 10/31/2016

// Show how arrays are passed by reference to methods and cannot be
// replaced, only individual elements changed.

public class ArrayReference
{
    public static void main(String[] args)
    {
        // manually create an array holding 0-5
        
        // print the array out
        
        // call a method ReplaceArray on the array
        
        // print the array out
        
        // call a method ReplaceArrayElements on the array
        
        // print the array out
        
    }
    
    public static void ReplaceArray(int[] array)
    {
        // manually create an array2 containing 0,2,4,6,8 and
        // replace the entire array with array2
        
    }
    
    public static void ReplaceArrayElements(int[] array)
    {
        // manually create an array2 containing 0,2,4,6,8 and
        // replace the individual elements with those from array2
        
    }
}