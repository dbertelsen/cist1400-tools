// L17 - Random numbers (and arrays) - 3 / 13 / 2017

// Create a simulation that will roll a die with a user-specified number of
// sides (make sure it is at least 2) 1000 times and return the results for each
// side as a count and percentage of the total. Output should be formatted as
// below; you may see some offsets in your text alignment as number of sides
// increases. (You could fix this with printf...)

/* Example:

   How many sides are on your die? _6_

   Results for 1000 rolls of a 6-sided die:
   Side   Rolls   Percent
   1      152     15.20 %
   2      165     16.50 %
   3      170     17.00 %
   4      163     16.30 %
   5      172     17.20 %
   6      178     17.80 %
*/


