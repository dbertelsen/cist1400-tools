// L14 (review) - Method writing and use - 3/1/2017

// write a program that takes a number between 1 and 10 (inclusive) from the
// user and then prompts for that many integers to be read from the user.
// After reading a given integer, print out the number, carat, three, an equals
// sign and the cube of the integer. Finding the cube should be performed in a
// method called findCube() that does not perform any output (i.e., it returns
// the cube as a double). Your output should be printed without a decimal value.

// Don't forget to document your methods!

// EXAMPLE OUTPUT:
// 
// Please enter the number of integers you wish to cube: -1
// Please enter the number of integers you wish to cube: 0
// Please enter the number of integers you wish to cube: 11
// Please enter the number of integers you wish to cube: 3

// Please enter an integer: -3
// 3^3 = 27

// Please enter an integer: 6
// 6^3 = 216

// Please enter an integer: 100
// 100^3 = 1000000

