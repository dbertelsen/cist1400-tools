// experiment with integer modulus (remainder function)

import java.util.Scanner;

public class IntModulus{

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args)
    {
        System.out.print("Enter the number you wish to divide by: ");
        int divisor = input.nextInt();
    
        for (int i = 0; i <= 2 * divisor; i = i + 1)
        {
            //TODO - make sure the print statement will output the correct number
            System.out.printf("%d %% %d = %d\n", i, );
        }
    }
}