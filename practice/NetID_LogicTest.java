/*    In-class assignment for 1/30/2017 - Logical expressions
 *
 *    Using the conditional and logical operators in java, write expressions
 *    using the given variables and operators.
 *
 *    You may use extra operators and parenthesis as needed. You may not change
 *    the value of B1-B4.
 *
 *    Example:
 *
 *    B1 && B4 : true
 *
 */

 public class NetID_LogicTest
 {
     public static void main(String[] args)
     {
         final boolean B1 = true;
         final boolean B2 = false;
         final boolean B3 = false;
         final boolean B4 = true;

         // (1 pt) Write and expression using B1, B2 and && that evaluates to true.

         System.out.printf("");

         // (1 pt) Write and expression using B1, B2, B3 and || that evaluates to false.

         System.out.printf("");


         // (1 pt) Write and expression using B1, B3, B4, ^ and && that evaluates to true.

         System.out.printf("");


         // (1 pt) Write and expression using B2, B3, B4, ! and && that evaluates to true.

         System.out.printf("");


         // (1 pt) Write and expression using B1, B2, B3, B4 !, && and | that evaluates to false.

         System.out.printf("");


     } // end main

 } // end class
