// L21 and beyond - 11/7/2016 - Connect Four (TM) clone using 2D arrays

/* Basic board layout

| | | | | | | |
| | | | | | | |
| | | | | | | |
| | | | | | | |
| | |o| | | | |
| |x|x|o| | | |
===============
 0 1 2 3 4 5 6

 */
 
import java.util.Scanner;
 
public class FourInARow
{
    static Scanner input = new Scanner(System.in);
    
    private final int BOARD_HEIGHT = ???;
    private final int BOARD_WIDTH = ???;

    public static void main(String[] args)
    {
        
        // loop to repeat gameplay
        do
        {
            
            // get a move
                // get a move if not valid
            
            // try to move
                // get a move if not valid
            
            // check if win
            
            
            
      
        } while (keepPlaying());
    } // end main
    
    public static boolean keepPlaying()
    {
        do
            {
            System.out.print("\nDo you want to play again? ");
                    char answer = input.next().charAt(0);
                    if (answer == 'y' || answer == 'Y')
                    {
                        return true;
                    }
                    else if (answer == 'n' || answer == 'N')
                    {
                        return false;
                    }
            } while (true);
    }
    
} // end class


/* 
    Additional notes:
    =================
    
    Moves begin as lower case; switch to upper case to mark a win.
    
    Hint for checking wins: you can do math on chars!
    x + x + x + x == 4 * x is true
    x + o + x + x != 4 * x is false
    
    Be careful with array ranges when checking!
    
*/