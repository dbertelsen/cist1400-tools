// 1/25/2017 - Practice using and outputting conditional operators.
// Note: This exercise creates what is known as a truth table - these are useful!

import java.util.Scanner;

public class ConditionalOperators{

    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args)
    {
        int selection = -1;

        do {
            System.out.println("Welcome to the CIST1400 ConditionalOperators Tester!\n);
            System.out.print("Enter 1 for Conditional AND or 2 for conditional OR: ")
            selection = input.nextInt();

            if (selection < 1 || selection > 2)
            {
                System.out.println(" ERROR: invalid number, try again");
            } // end error printout code
            
        } while (selection !=1 && selection !=2);


        if(selection == 1)
        {
            System.out.println(" -- Conditional AND (&&) -- ");
            System.out.printf(" %s: %b\n", "false && false", (false && false));
            //TODO - other 3 cases
            System.out.println();
        } // end conditional AND code
        

        //TODO - create case for conditional OR

    } // end main
    
} // end class