// L19 - Searching and Sorting - 3/7/2017

// Implement a linear search algorithm.
// It should create an array of length SIZE and fill it with positive integer
// values up to SIZE squared. It will then look for a random value between
// 1 and SIZE squared. Output should be as follows:

// Your search value of 3 was first found at index 6.

// The function will return -1 if the value was not found.

// BONUS: Add in functionality to allow use to enter a value to search for.

import java.security.SecureRandom;

public class Search_linear
{
    private static final int SIZE = 10;
    
    
    public static void main(String[] args)
    {
        int[] myArray = new int[SIZE];
        SecureRandom rand = new SecureRandom();

        System.out.print("Before filling: ");
        printArray(myArray);
        System.out.println();
        
        for (int i = 0; i < myArray.length; i++)
        {
            myArray[i] = rand.nextInt(SIZE * SIZE + 1);
        }
        
        System.out.print("After filling: ");
        printArray(myArray);
        System.out.println();
        
        int target = rand.nextInt(SIZE * SIZE + 1);
        
        // TODO - Add code to print out search results as described above.
        

    } // end main
    
    public static int linearSearch(int[] values, int searchKey)
    {
        int comparisionCount = 0;
        
        
        
        System.out.printf("comparisonCount=%d\n", comparisionCount);
        return 0;
    } // end linearSearch method
    
    public static void printArray(int[] values)
    {
        System.out.print("Array Contents: [");
        for (int value : values)
        {
            System.out.printf(" %d,", value);
        }
        System.out.println(" ]");
        
    } // end printArray method
    
} // end class