// Lecture 13 Review (L15) - 3/6/2017

// Take in an int myNum from the user and use that to print an isoceles
// triangle made of * that has a base that is wide and is myNum lines tall
// the point (first line) should be two asterisks ** and each following
// row is two asterisks wider and centered on the previous.

// Hint: for loops are your friend!

/* Example

myNum = 5

    **
   ****
  ******
 ********
**********

myNum = 10

         **
        ****
       ******
      ********
     **********
    ************
   **************
  ****************
 ******************
********************

Good luck! */

import java.util.Scanner;

public class Isoceles
{
    public static void main(String[] args)
    {
    
    
    
    } // end main
} // end class
