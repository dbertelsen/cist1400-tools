// Lecture 13 Review (L15) - 3/6/2017

// Print an asterisk right triangle with the point to the lower left
// that is the height of an integer taken from the user

/* Example

myNum = 6

     *
    **
   ***
  ****
 *****
******
*/


import java.util.Scanner;

public class RightTri
{
    public static void main(String[] args)
    {
        
        int height;
        Scanner input = new Scanner(System.in);
        
        do
        {
            System.out.print("Enter a height for your triangle (1-50): ");
            height = input.nextInt();
        } while ();
        
        for ()
        {
            for ()
            {
                System.out.print(" ");
            }
            
            
            for ()
            {
                System.out.print("*");
            }
            System.out.println();
        }
        
        
    } // end main
} // end class
